import { writable, derived } from "svelte/store";

export const products = writable([
  {
    id: 1,
    name: "Daging Sapi",
    price: 90000,
    shortdesc: "Daging sapi dari peternakan dengan savana yang hijau dan segar",
    url: "images/sapi.png",
  },
  {
    id: 2,
    name: "Tempe",
    price: 4000,
    shortdesc: "Tempe dari kedelai pilihan yang dibesarkan seperti anak sendiri",
    url: "images/tempe.png",
  },
  {
    id: 3,
    name: "Daging Ayam",
    price: 60000,
    shortdesc: "Daging ayam pilihan yang dipotong dengan Bismillah",
    url: "images/ayam.png",
  },
  {
    id: 4,
    name: "Beras",
    price: 25000,
    shortdesc: "Beras putih dan pulen dari sawah langsung",
    url: "images/beras.png",
  },
  {
    id: 5,
    name: "Kentang",
    price: 40000,
    shortdesc: "kentang yang ditanam dengan pupuk organik",
    url: "images/kentang.png",
  },
  {
    id: 6,
    name: "Telur",
    price: 45000,
    shortdesc: "Telur ayam organik yang diambil dari ayam pilihan",
    url: "images/telur.png",
  },
]);

export const cart = writable([]);

/* export const loading =  writable(true);
export const isAuthenticated = writable(false);
export const user =  writable({});
export const popupOpen = writable(false)
export const error = writable();

export const tasks = writable([]);

export const user_tasks = derived([tasks, user], ([$tasks, $user]) => {

    let logged_in_user_tasks = [];

    if($user && $user.email){
        logged_in_user_tasks = $tasks.filter((task) => task.user === $user.email)
    }

    return logged_in_user_tasks;
}); */
